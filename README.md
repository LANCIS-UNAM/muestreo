# Muestreo

Esta app pretende brindar retroalimentación a proyectos transdisciplinarios
acerca de su impacto según lo perciben los _stakeholders_ y acercar a estos últimos
a investigadores ejecutando proyectos en sus localidades.

Son dos funciones principales.

1. Acopiar cotidianamente respuestas a preguntas, e.g.: ¿hoy hubo suministro de agua? El agregado de respuestas georeferenciadas a esta pregunta ejemplo revelaría qué regiones tienen carencias de suministro de agua.

2. Vincular a personas con investigadores. La app muestra un menú de preguntas para responder cotidianamente. Elegir esas preguntas también afirma los intereses del usuario. Para ciertas preguntas habrá proyectos transdisciplinarios ejecutándose en la región del usuario.


